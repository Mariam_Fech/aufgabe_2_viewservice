package de.htwberlin.viewService.impl;

import java.util.ArrayList;
import java.util.Scanner;

import de.htwberlin.viewService.inter.ViewServiceInt;

public class ViewService implements ViewServiceInt {
	
	private int gameVariety;
	private Scanner scan;
	
	private int inputNr() {
		 scan = new Scanner(System.in);
		 int input = scan.nextInt();
		 scan.close();
		 return input;
	}
	
	private String inputString() {
		 scan = new Scanner(System.in);
		 String input = scan.nextLine();
		 scan.close();
		 return input;
	}
	
	@Override
	public ArrayList<String> outputConfig() {
		// TODO Auto-generated method stub
		ArrayList<String> s = new ArrayList<String>();
		String str = "dummyuser";
		String var = "1";
		s.add(str);
		s.add(var);
		System.out.println("Hello dummyuser, lets play MauMau!");
		return s;
	}
	@Override
	public void outputCardsPlayer(ArrayList<String> suits, ArrayList<String> ranks) {
		// TODO Auto-generated method stub
		System.out.println("yours cards:");
		System.out.println(suits);
		System.out.println(ranks);
		
	}
	@Override
	public void outputOpenCard(String suit, String rank) {
		// TODO Auto-generated method stub
		System.out.println("open card: "+suit+" "+rank);
	}
	@Override
	public int outputWish() {
		// TODO Auto-generated method stub
		System.out.println("wish a suit");
		return 1;
	}
	@Override
	public int outputPlaceCard() {
		// TODO Auto-generated method stub
		System.out.println("It's your turn, make your move");
		return 1;
	}
	@Override
	public void outputInvalidMove(String s) {
		// TODO Auto-generated method stub
		System.out.println("invalid move, choose other card");
		
	}
	@Override
	public void outputMau(String namePlayer) {
		// TODO Auto-generated method stub
		System.out.println("dummy says Mau");
	}
	@Override
	public void outputMauMau(String namePlayer) {
		// TODO Auto-generated method stub
		System.out.println("dummy says MauMau. Game over.");
	}
	@Override
	public void outputScore(ArrayList<Integer> scores) {
		// TODO Auto-generated method stub
		System.out.println("You win! your points: 0; bot points:40");
	}
	@Override
	public int outputPlayAgain() {
		// TODO Auto-generated method stub
		System.out.println("Play again? Yes=1 No=2");
		return 1;
	}
	
	
	
	
	

	

}
