package de.htwberlin.viewService.inter;

import java.util.ArrayList;

/**
 * The Interface ViewService.
 */
public interface ViewServiceInt {

	/**
	 * Displays initial interface
	 * asking for user's name
	 * asking for choice of game variety 
	 * reading user's inputs
	 *
	 * @return ArrayList<String> List of users inputs: [0]=name, [1]=game variety
	 */
	public ArrayList<String> outputConfig();
	
	/**
	 * Displays users cards 
	 * @param suits List of Suits of the Cards
	 * @param ranks List of ranks of the Cards
	 */
	public void outputCardsPlayer(ArrayList<String> suits,ArrayList <String> ranks);
	
	/**
	 * Displays open card
	 * @param suit Suit of the open Card:
	 * 1=hearts,2=diamonds,3=clubs,4=spades
	 * @param rank Rank of the open Card:
	 * 6=six,..10=ten, 11=jack, 12=queen, 13=king, 14=ace
	 */
	public void outputOpenCard(String suit, String rank);
	
	/**
	 * Displays all suits and
	 * asking to choose one of them
	 * reading the choice
	 * @return int choice of suit by user:
	 * 1 = hards, 2 = diamonds, 3 = clubs, 4 = spades
	 */
	public int outputWish();
	
	/**
	 * Asks to place a card
	 * reading user's choice
	 * @return int index of chosen card in players deck List
	 */
	public int outputPlaceCard();
	
	/**
	 * Displays an error message signalling that the move is not allowed
	 * @param s error Message
	 */
	public void outputInvalidMove(String s);
	
	/**
	 * Displays: namePlayer says "Mau" 
	 * @param namePlayer the name of the Player who is saying "Mau"
	 */
	public void outputMau(String namePlayer);
	
	/**
	 * Displays: namePlayer says "MauMau" 
	 * @param namePlayer the name of the Player who is saying "MauMau"
	 */
	public void outputMauMau(String namePlayer);
	
	/**
	 * Displays the endscore of the game
	 * @param scores List with endscores: [0]=user, [1]=bot...
	 */
	public void outputScore(ArrayList<Integer> scores);
	
	/**
	 * Asking if the user wants to play again
	 * reading user's choice
	 * @return int answer 1=yes,2=no
	 */
	public int outputPlayAgain();
	
}
